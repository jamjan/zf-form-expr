<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\LabelAttributeEntity as LabelAttributeEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class LabelAttributeFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('label_attribute');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new LabelAttributeEntity())
            ->setAttribute('class', 'label_attribute-item')
        ;

        $this->setLabel('Label Attributes');

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => 'Attribute Name',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(required)',
            ],
        ]);

        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Attribute Value',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_label_attribute',
            'options' => [
                'label' => 'Remove Label Attribute',
            ],
            'attributes' => [
                'id' => 'remove-attribute',
                'class' => 'btn btn-xs btn-danger pull-right',
                'onClick' => 'javascript:formCreate.remove($(this),"label_attribute");return false;',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => true,
            ],
            'value' => [
                'required' => false,
            ],
        ];
    }
}
