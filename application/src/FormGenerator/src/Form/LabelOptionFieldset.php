<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\LabelOptionEntity as LabelOptionEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class LabelOptionFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('label_option');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new LabelOptionEntity())
            ->setAttribute('class', 'label_option-item')
        ;

        $this->setLabel('Label Options');

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => 'Option Name',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(required)',
            ],
        ]);

        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Option Value',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_label_option',
            'options' => [
                'label' => 'Remove Label Option',
            ],
            'attributes' => [
                'id' => 'remove-attribute',
                'class' => 'btn btn-xs btn-danger pull-right',
                'onClick' => 'javascript:formCreate.remove($(this),"label_option");return false;',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => true,
            ],
            'value' => [
                'required' => false,
            ],
        ];
    }
}
