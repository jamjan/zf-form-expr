<?php

namespace FormGenerator\Entity;

class ElementRadioEntity
{

    /**
     * @var ElementRadioBasicEntity
     */
    protected $basic;

    /**
     * @var ElementRadioValueOptionsEntity
     */
    protected $value_options;

    /**
     * @return ElementRadioBasicEntity
     */
    public function getBasic()
    {
        return $this->basic;
    }

    /**
     * @param mixed $basic
     * @return ElementRadioEntity
     */
    public function setBasic($basic)
    {
        $this->basic = $basic;
        return $this;
    }

    /**
     * @return ElementRadioValueOptionsEntity
     */
    public function getValueOptions()
    {
        return $this->value_options;
    }

    /**
     * @param mixed $value_options
     * @return ElementRadioEntity
     */
    public function setValueOptions($value_options)
    {
        $this->value_options = $value_options;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data['basic'] = $this->getBasic();
        $data['value_options'] = $this->getValueOptions();
        return $data;
    }
}
