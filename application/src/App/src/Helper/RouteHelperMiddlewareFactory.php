<?php
/**
 * @see       https://github.com/zendframework/zend-expressive-helpers for the canonical source repository
 * @copyright Copyright (c) 2015-2017 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   https://github.com/zendframework/zend-expressive-helpers/blob/master/LICENSE.md New BSD License
 */

namespace App\Helper;

use Psr\Container\ContainerInterface;
use Zend\Expressive\Helper\Exception\MissingHelperException;

class RouteHelperMiddlewareFactory
{
    /**
     * Create and return a UrlHelperMiddleware instance.
     *
     * @param ContainerInterface $container
     * @return RouteHelperMiddleware
     * @throws MissingHelperException if the RouteHelper service is missing
     */
    public function __invoke(ContainerInterface $container)
    {
        if (! $container->has(RouteHelper::class)) {
            throw new MissingHelperException(sprintf(
                '%s requires a %s service at instantiation; none found',
                RouteHelperMiddleware::class,
                RouteHelper::class
            ));
        }

        return new RouteHelperMiddleware($container->get(RouteHelper::class));
    }
}
