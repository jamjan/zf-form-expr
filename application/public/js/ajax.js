
var Ajax = function(name){
    SecalithZfAjax.apply(this,arguments);
    this.name = name;
}

Ajax.prototype = SecalithZfAjax.prototype;
Ajax.prototype.constructor = Ajax;

Ajax.prototype.getValidators = function(jqButtonElement) {
    window[this.name].removeFieldset(jqButtonElement,"validator_option");
    window[this.name].reindexLegend(jqButtonElement,"validator_option");
};

var ajax = new Ajax("formCreate");
