<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\LabelEntity as LabelEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class LabelFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('label');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new LabelEntity())
            ->setAttribute('class', 'label-item')
        ;

        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Label',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'value' => [
                'required' => false,
            ],
        ];
    }
}
