<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\ElementUrlEntity as Entity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class ElementUrlFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('element_url');

        $this->setHydrator(new ClassMethodsHydrator(true))
            ->setObject(new Entity())
            ->setAttribute('class', 'element_url-collection')
        ;

        $this->add([
            'type' => 'FormGenerator\Form\ElementUrlBasicFieldset',
            'name' => 'basic',
            'attributes' => [
                'class' => 'element_url_basic-collection container-element_url_basic-collection',
            ],
        ]);

    }

    public function getInputFilterSpecification()
    {
        return [];
    }
}
