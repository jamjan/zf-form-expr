<?php
return [
'form_elements' => [
        'spec' => [
            'create' => [
                'name' => 'generator_form_test',
                'type' => 'Zend\\Form\\Form',
                'attributes' => [
                    'method' => 'post',
                    'action' => '/',
                ],
                'options' => [
                    'prefer_form_input_filter' => true,
                ],
                'fieldsets' => [
                    0 =>[
                        'spec' => [
                            'name' => 'create',
                            'type' => 'Zend\Form\Fieldset',
                            'hydrator' => 'Zend\Hydrator\ClassMethods',
                            // 'object' => 'Generator\Form\Entity\BasicEntity',
                            'attributes' => [
                                'class' => 'form-inline',
                            ],
                            'options' => [
                                'label' => 'New Form',
                            ],
                            'fieldsets' => [
                                0 => [
                                    'spec' => [
                                        'name' => 'basic',
                                        'type' => 'Zend\Form\Fieldset',
                                        'hydrator' => 'Zend\Hydrator\ClassMethods',
                                        //'object' => 'FormGenerator\Model\ElementEntity',
                                        'attributes' => [
                                            'class' => 'form-inline',
                                        ],
                                        'options' => [
                                            'label' => 'Basic Data',
                                        ],
                                        'elements' => [
                                            [
                                                'spec' => [
                                                    'name' => 'form_id',
                                                    'options' => [
                                                        'label' => 'Form Id',
                                                        'label_attributes' => [
                                                            'class'  => 'form-group'
                                                        ],
                                                    ],
                                                    'attributes' => [
                                                        'type' => 'text',
                                                        'class' => 'form-control',
                                                    ],
                                                ],
                                            ],
                                            [
                                                'spec' => [
                                                    'name' => 'form_name',
                                                    'options' => [
                                                        'label' => 'Form name',
                                                        'label_attributes' => [
                                                            'class'  => 'form-group'
                                                        ],
                                                    ],
                                                    'attributes' => [
                                                        'type' => 'text',
                                                        'class' => 'form-control',
                                                    ],
                                                ],
                                            ],
                                            [
                                                'spec' => [
                                                    'name' => 'form_method',
                                                    'type' => 'Zend\Form\Element\Select',
                                                    'options' => [
                                                        'label' => 'Method',
                                                        'label_attributes' => [
                                                            'class'  => 'form-group'
                                                        ],
                                                        'value_options' => [
                                                            'post' => 'POST',
                                                            'get' => 'GET',
                                                        ],
                                                    ],
                                                    'attributes' => [
                                                        'class' => 'form-control',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ], // 0

                                1 => [
                                    'spec' => [
                                        'name' => 'add_element',
                                        'type' => 'button',
                                        'options' => [
                                            'label' => 'Add Element',
                                            'label_attributes' => [
                                                'for'  => 'create-fieldsets-add_element',
                                            ],
                                        ],
                                        'attributes' => [
                                            'class' => 'btn btn-default btn-create',
                                            'onClick' => 'formulaire.add(jQuery(this),".container-elements",".container-element");',
                                            'id'  => 'add-elements',
                                        ],
                                    ],
                                ], // 1

                                2 => [
                                    'spec' => [
                                        'name' => 'elements',
                                        'type' => 'Zend\Form\Element\Collection',
                                        'hydrator' => 'Zend\Hydrator\ClassMethods',
                                        'object' => 'Zend\Form\Fieldset',
                                        'attributes' => [
                                            'class' => 'container-elements',
                                        ],
                                        'options' => [
                                            'label' => 'Elements',
                                            'count' => 0,
                                            'should_create_template' => true,
                                            'allow_add' => true,
                                            'allow_remove' => true,
                                            'create_new_objects' => true,
                                            'template_placeholder' => '__placeholder__',
                                            'target_element' => [
                                                'type' => 'Zend\Form\Fieldset',
                                                'name' => 'element',
                                                'hydrator' => 'Zend\Hydrator\ClassMethods',
                                                //'object' => 'Generator\Form\Entity\ElementEntity',
                                                'attributes' => [
                                                    'class' => 'container-element',
                                                ],
                                                'options' => [
                                                    'label' => 'Element',
                                                ],
                                                'elements' => [
                                                    0 =>[
                                                        'spec' => [
                                                            'name' => 'name',
                                                            'type' => 'text',
                                                            'options' => [
                                                                'label' => 'Element Name',
                                                                'label_attributes' => [
                                                                    'for'  => 'create-fieldsets-name',
                                                                ],
                                                            ],
                                                            'attributes' => [
                                                                'value' => 'content',
                                                                'type' => 'text',
                                                                'class' => 'form-control',
                                                                'id'  => 'create-fieldsets-name',
                                                            ],
                                                        ],
                                                    ],
                                                    1 =>[
                                                        'spec' => [
                                                            'name' => 'label',
                                                            'type' => 'text',
                                                            'options' => [
                                                                'label' => 'Element Label',
                                                                'label_attributes' => [
                                                                    'for'  => 'create-fieldsets-name',
                                                                ],
                                                            ],
                                                            'attributes' => [
                                                                'value' => 'content',
                                                                'type' => 'text',
                                                                'class' => 'form-control',
                                                                'id'  => 'create-fieldsets-name',
                                                            ],
                                                        ],
                                                    ],
                                                    2 => [
                                                        'spec' => [
                                                            'type' => 'select',
                                                            'name' => 'element_type',
                                                            'options' => [
                                                                'label' => 'Element Type',
                                                                'label_attributes' => [
                                                                    'for'  => 'create-basic-element_type',
                                                                ],
                                                                'value_options' => [
                                                                    'text' => 'Text',
//                                                                   'textarea' => 'Textarea',
//                                                                   'radio' => 'Radio',
//                                                                   'select' => 'Select',
//                                                                   'checkbox' => 'Checkbox',
                                                                ],
                                                            ],
                                                            'attributes' => [
                                                                'class' => 'form-control',
                                                                'id'  => 'create-basic-file_overwrite',
                                                            ],
                                                        ],
                                                    ], // 2
                                                    3 => [
                                                        'spec' => [
                                                            'type' => 'checkbox',
                                                            'name' => 'is_required',
                                                            'options' => [
                                                                'label' => 'Required',
                                                                'label_attributes' => [
                                                                    'for' => 'create-fieldsets-is_required',
                                                                ],
                                                                'value_options' => [
                                                                    [
                                                                        'value' => 'is_required',
                                                                        'label' => 'Create',
                                                                        'selected' => true,
                                                                        'disabled' => false,
                                                                    ],
                                                                ], // value_options
                                                            ],
                                                            'attributes' => [
                                                                'type' => 'checkbox',
                                                                'class' => 'form-cohntrol',
                                                                'id'  => 'create-fieldsets-is_required',
                                                            ],
                                                        ],
                                                    ], // 3
                                                    4 => [
                                                        'spec' => [
                                                            'name' => 'remove_element',
                                                            'type' => 'button',
                                                            'options' => [
                                                                'label' => 'Remove Element',
                                                                'label_attributes' => [
                                                                    'for'  => 'create-fieldsets-fieldsets-remove_element',
                                                                ],
                                                            ],
                                                            'attributes' => [
                                                                'value' => 'Remove Element',
                                                                'class' => 'btn btn-warning btn-sm pull-right',
                                                                'onClick' => 'formulaire.remove(jQuery(this),".container-elements",".container-element");',
                                                                'id'  => 'create-route-fieldsets-remove_element',
                                                            ],
                                                        ],
                                                    ], // 4
                                                    5 => [
                                                        'spec' => [
                                                            'name' => 'add_validator',
                                                            'type' => 'button',
                                                            'options' => [
                                                                'label' => 'Add Validator',
                                                                'label_attributes' => [
                                                                    'for'  => 'create-fieldsets-add_element',
                                                                ],
                                                            ],
                                                            'attributes' => [
                                                                'class' => 'btn btn-sm btn-default btn-create',
                                                                'onClick' => 'formulaire.add(jQuery(this),".container-validators",".container-validator");',
                                                                'id'  => 'add-elements',
                                                            ],
                                                        ],
                                                    ], // 5
                                                    6 => [
                                                        'spec' => [
                                                            'name' => 'add_filter',
                                                            'type' => 'button',
                                                            'options' => [
                                                                'label' => 'Add Filter',
                                                                'label_attributes' => [
                                                                    'for'  => 'create-fieldsets-add_element',
                                                                ],
                                                            ],
                                                            'attributes' => [
                                                                'class' => 'btn btn-sm btn-default btn-create',
                                                                'onClick' => 'formulaire.add(jQuery(this),".container-filters",".container-filter");',
                                                                'id'  => 'add-filters-btn',
                                                            ],
                                                        ],
                                                    ], // 6
                                                ], // elements

                                                'fieldsets' => [
                                                    0 => [
                                                        'spec' => [
                                                            'name' => 'validators',
                                                            'type' => 'Zend\Form\Element\Collection',
                                                            'hydrator' => 'Zend\Hydrator\ClassMethods',
                                                            'object' => 'Zend\Form\Fieldset',
                                                            'attributes' => [
                                                                'class' => 'container-validators',
                                                            ],
                                                            'options' => [
                                                                'label' => 'Validators',
                                                                'count' => 0,
                                                                'should_create_template' => true,
                                                                'allow_add' => true,
                                                                'allow_remove' => true,
                                                                'create_new_objects' => true,
                                                                'template_placeholder' => '__placeholder__',
                                                                'target_element' => [
                                                                    'type' => 'Zend\Form\Fieldset',
                                                                    'name' => 'element',
                                                                    'hydrator' => 'Zend\Hydrator\ClassMethods',
                                                                    //'object' => 'Generator\Form\Entity\ElementEntity',
                                                                    'attributes' => [
                                                                        'class' => 'container-validator',
                                                                    ],
                                                                    'options' => [
                                                                        'label' => 'Validator',
                                                                    ],
                                                                    'elements' => [

                                                                        0 =>[
                                                                            'spec' => [
                                                                                'name' => 'name',
                                                                                'type' => 'text',
                                                                                'options' => [
                                                                                    'label' => 'Validator Name',
                                                                                    'label_attributes' => [
                                                                                        'for'  => 'create-fieldset-name',
                                                                                    ],
                                                                                ],
                                                                                'attributes' => [
                                                                                    'value' => 'content',
                                                                                    'type' => 'text',
                                                                                    'class' => 'form-control',
                                                                                    'id'  => 'create-fieldset-name',
                                                                                ],
                                                                            ],
                                                                        ],
                                                                    ],
                                                                ], // target_element
                                                            ], // options
                                                        ], // spec
                                                    ], // 0
                                                    1 => [
                                                        'spec' => [
                                                            'name' => 'filters',
                                                            'type' => 'Zend\Form\Element\Collection',
                                                            'hydrator' => 'Zend\Hydrator\ClassMethods',
                                                            'object' => 'Zend\Form\Fieldset',
                                                            'attributes' => [
                                                                'class' => 'container-filters',
                                                            ],
                                                            'options' => [
                                                                'label' => 'Filters',
                                                                'count' => 0,
                                                                'should_create_template' => true,
                                                                'allow_add' => true,
                                                                'allow_remove' => true,
                                                                'create_new_objects' => true,
                                                                'template_placeholder' => '__placeholder__',
                                                                'target_element' => [
                                                                    'type' => 'Zend\Form\Fieldset',
                                                                    'name' => 'element',
                                                                    'hydrator' => 'Zend\Hydrator\ClassMethods',
                                                                    //'object' => 'Generator\Form\Entity\ElementEntity',
                                                                    'attributes' => [
                                                                        'class' => 'container-filter',
                                                                    ],
                                                                    'options' => [
                                                                        'label' => 'Filter',
                                                                    ],
                                                                    'elements' => [

                                                                        0 =>[
                                                                            'spec' => [
                                                                                'name' => 'name',
                                                                                'type' => 'text',
                                                                                'options' => [
                                                                                    'label' => 'Filter Name',
                                                                                    'label_attributes' => [
                                                                                        'for'  => 'create-fieldset-name',
                                                                                    ],
                                                                                ],
                                                                                'attributes' => [
                                                                                    'value' => 'content',
                                                                                    'type' => 'text',
                                                                                    'class' => 'form-control',
                                                                                    'id'  => 'create-fieldset-name',
                                                                                ],
                                                                            ],
                                                                        ],
                                                                    ],
                                                                ], // target_element
                                                            ], // options
                                                        ], // spec
                                                    ], // 1
                                                ], // fieldsets
                                            ], // target_element
                                        ], // options
                                    ], // spec
                                ], // 2
                            ], // fieldsets
                        ], // spec
                    ], // 0
                ], // fieldsets [create]
            ], // create
        ], // spec
    ], // form_elements
];
