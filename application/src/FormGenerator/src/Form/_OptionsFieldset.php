<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\OptionsEntity as OptionsEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class OptionsFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('options');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new OptionsEntity())
        ;

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'add_option',
            'options' => [
                'label' => 'Add Option',
            ],
            'attributes' => [
                'id' => 'add-option',
                'class' => 'btn',
                'onClick' => 'javascript:add_option($(this));',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'option',
            'options' => [
                'label' => 'Option',
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'target_element' => [
                    'type' => 'FormGenerator\Form\OptionFieldset',
                ],
            ],
            'attributes' => [
                'class' => 'element-option',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [];
    }
}
