<?php

namespace FormGenerator\Entity;

class ElementSelectEntity
{

    /**
     * @var ElementSelectBasicEntity|null
     */
    protected $basic;

    /**
     * @var ElementSelectValueOptionsEntity|null
     */
    protected $value_options;

    /**
     * @return ElementSelectBasicEntity|null
     */
    public function getBasic()
    {
        return $this->basic;
    }

    /**
     * @param mixed $basic
     * @return ElementSelectEntity
     */
    public function setBasic($basic)
    {
        $this->basic = $basic;
        return $this;
    }

    /**
     * @return ElementSelectValueOptionsEntity|null
     */
    public function getValueOptions()
    {
        return $this->value_options;
    }

    /**
     * @param mixed $value_options
     * @return ElementSelectEntity
     */
    public function setValueOptions($value_options)
    {
        $this->value_options = $value_options;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data['basic'] = $this->getBasic();
        $data['value_options'] = $this->getValueOptions();
        return $data;
    }
}
