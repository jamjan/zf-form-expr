<?php

namespace FormGenerator\Entity;

class ElementUrlEntity
{

    /**
     * @var ElementUrlBasicEntity|null
     */
    protected $basic;

    /**
     * @return ElementUrlBasicEntity|null
     */
    public function getBasic()
    {
        return $this->basic;
    }

    /**
     * @param mixed $basic
     * @return ElementUrlEntity
     */
    public function setBasic($basic)
    {
        $this->basic = $basic;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data['basic'] = $this->getBasic();
        return $data;
    }
}
