<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\FormBasicEntity as FormBasicEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;
use FormGenerator\Hydrator\InputNameStrategy;

class FormBasicFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('basic');

        $hydrator = new ClassMethodsHydrator(true);
        $hydrator->addStrategy('name', new InputNameStrategy());

        $this->setHydrator($hydrator)
            ->setObject(new FormBasicEntity())
        ;

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => 'Name',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);

        $this->add([
            'name' => 'id',
            'options' => [
                'label' => 'Id',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Select',
            'name' => 'method',
            'options' => [
                'label' => 'Method',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
//                'empty_option' => '--select--',
                'value_options' => [
                    'post' => 'POST (recommended)',
                    'get' => 'GET',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);

    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => false,
            ],
            'id' => [
                'required' => false,
            ],
            'method' => [
                'required' => true,
            ],
        ];
    }
}
