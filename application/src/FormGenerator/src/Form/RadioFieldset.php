<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\RadioEntity as RadioEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class RadioFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('element_radio');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new RadioEntity())
            ->setAttribute('class', 'label-item')
        ;

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => 'Name',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);
        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Value',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(required)',
            ],
        ]);

    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => false,
            ],
            'value' => [
                'required' => true,
            ],
        ];
    }
}
