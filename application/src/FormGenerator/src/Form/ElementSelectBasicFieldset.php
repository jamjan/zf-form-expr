<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\ElementSelectBasicEntity as Entity;
use FormGenerator\Hydrator\InputNameStrategy;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class ElementSelectBasicFieldset extends Fieldset implements InputFilterProviderInterface
{

    public function __construct()
    {
        parent::__construct('element_select_basic');

        $hydrator = new ClassMethodsHydrator(true);
        $hydrator->addStrategy('name', new InputNameStrategy());

        $this->setHydrator($hydrator)
            ->setObject(new Entity())
            ->setAttribute('class', 'element_select_basic-item')
        ;

        $this->setLabel('Element Specific Settings');

        $this->add([
            'name' => 'empty_option',
            'options' => [
                'label' => 'Empty Option',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(string)',
            ],
        ]);

        $this->add([
            'name' => 'use_hidden_element',
            'options' => [
                'label' => 'Use Hidden Element',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(boolean)',
            ],
        ]);

        $this->add([
            'name' => 'unselected_value',
            'options' => [
                'label' => 'Unselected Value',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(string)',
            ],
        ]);

        $this->add([
            'name' => 'disable_inarray_validator',
            'options' => [
                'label' => 'Disable inArray Validator',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(boolean)',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'empty_option' => [
                'required' => false,
            ],
            'use_hidden_element' => [
                'required' => false,
            ],
            'unselected_value' => [
                'required' => false,
            ],
            'disable_inarray_validator' => [
                'required' => false,
            ],
        ];
    }
}
