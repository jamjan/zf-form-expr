<?php
namespace FormGenerator\Entity;

class ValidatorMetadataEntity
{
    /**
     * @var array
     */
    protected $data = [
        '__all__' => [
            'breakchainonfailure' => 'bool',
            'message' => 'string',
            'messagelength' => 'int',
            'valueobscured' => 'bool',
            'translatortextdomain' => 'string',
            'translatorenabled' => 'bool',
        ],
        'Zend\Validator\Barcode\Codabar' => [],
        'Zend\Validator\Barcode\Code128' => [],
        'Zend\Validator\Barcode\Code25interleaved' => [],
        'Zend\Validator\Barcode\Code25' => [],
        'Zend\Validator\Barcode\Code39ext' => [],
        'Zend\Validator\Barcode\Code39' => [],
        'Zend\Validator\Barcode\Code93ext' => [],
        'Zend\Validator\Barcode\Code93' => [],
        'Zend\Validator\Barcode\Ean12' => [],
        'Zend\Validator\Barcode\Ean13' => [],
        'Zend\Validator\Barcode\Ean14' => [],
        'Zend\Validator\Barcode\Ean18' => [],
        'Zend\Validator\Barcode\Ean2' => [],
        'Zend\Validator\Barcode\Ean5' => [],
        'Zend\Validator\Barcode\Ean8' => [],
        'Zend\Validator\Barcode\Gtin12' => [],
        'Zend\Validator\Barcode\Gtin13' => [],
        'Zend\Validator\Barcode\Gtin14' => [],
        'Zend\Validator\Barcode\Identcode' => [],
        'Zend\Validator\Barcode\Intelligentmail' => [],
        'Zend\Validator\Barcode\Issn' => [],
        'Zend\Validator\Barcode\Itf14' => [],
        'Zend\Validator\Barcode\Leitcode' => [],
        'Zend\Validator\Barcode' => [
            'adapter' => 'string', // this is the validator adapter name to use
            'useChecksum' => 'bool',
        ],
        'Zend\Validator\Barcode\Planet' => [],
        'Zend\Validator\Barcode\Postnet' => [],
        'Zend\Validator\Barcode\Royalmail' => [],
        'Zend\Validator\Barcode\Sscc' => [],
        'Zend\Validator\Barcode\Upca' => [],
        'Zend\Validator\Barcode\Upce' => [],
        'Zend\Validator\Between' => [
            'inclusive' => 'bool',
            'max' => 'int',
            'min' => 'int',
        ],
        'Zend\Validator\Bitwise' => [
            'control' => 'int',
            'operator' => 'string',
            'strict' => 'bool',
        ],
        'Zend\Validator\Callback' => [
            'callback' => 'string',
        ],
        'Zend\Validator\CreditCard' => [
            'type' => 'string',
            'service' => 'string',
        ],
        'Zend\Validator\Csrf' => [
            'name' => 'string',
            'salt' => 'string',
            'timeout' => 'int',
        ],
        'Zend\Validator\Date' => [
            'format' => 'string',
        ],
        'Zend\Validator\DateStep' => [
            'format' => 'string',
            'basevalue' => 'string|int',
        ],
        'Zend\Validator\Db\NoRecordExists' => [
            'table' => 'string',
            'schema' => 'string',
            'field' => 'string',
            'exclude' => 'string',
        ],
        'Zend\Validator\Db\RecordExists' => [
            'table' => 'string',
            'schema' => 'string',
            'field' => 'string',
            'exclude' => 'string',
        ],
        'ZF\ContentValidation\Validator\DbNoRecordExists' => [
            'adapter' => 'string',
            'table' => 'string',
            'schema' => 'string',
            'field' => 'string',
            'exclude' => 'string',
        ],
        'ZF\ContentValidation\Validator\DbRecordExists' => [
            'adapter' => 'string',
            'table' => 'string',
            'schema' => 'string',
            'field' => 'string',
            'exclude' => 'string',
        ],
        'Zend\Validator\Digits' => [],
        'Zend\Validator\EmailAddress' => [
            'allow' => 'int',
            'useMxCheck' => 'bool',
            'useDeepMxCheck' => 'bool',
            'useDomainCheck' => 'bool',
        ],
        'Zend\Validator\Explode' => [
            'valuedelimiter' => 'string',
            'breakonfirstfailure' => 'bool',
        ],
        'Zend\Validator\File\Count' => [
            'max' => 'int',
            'min' => 'int',
        ],
        'Zend\Validator\File\Crc32' => [
            'algorithm' => 'string',
            'hash' => 'string',
            'crc32' => 'string',
        ],
        'Zend\Validator\File\ExcludeExtension' => [
            'case' => 'bool',
            'extension' => 'string',
        ],
        'Zend\Validator\File\ExcludeMimeType' => [
            'disableMagicFile' => 'bool',
            'magicFile' => 'string',
            'enableHeaderCheck' => 'bool',
            'mimeType' => 'string',
        ],
        'Zend\Validator\File\Exists' => [
            'directory' => 'string',
        ],
        'Zend\Validator\File\Extension' => [
            'case' => 'bool',
            'extension' => 'string',
        ],
        'Zend\Validator\File\FilesSize' => [
            'max' => 'int',
            'min' => 'int',
            'size' => 'int',
            'useByteString' => 'bool',
        ],
        'Zend\Validator\File\Hash' => [
            'algorithm' => 'string',
            'hash' => 'string',
        ],
        'Zend\Validator\File\ImageSize' => [
            'maxHeight' => 'int',
            'minHeight' => 'int',
            'maxWidth' => 'int',
            'minWidth' => 'int',
        ],
        'Zend\Validator\File\IsCompressed' => [
            'disableMagicFile' => 'bool',
            'magicFile' => 'string',
            'enableHeaderCheck' => 'bool',
            'mimeType' => 'string',
        ],
        'Zend\Validator\File\IsImage' => [
            'disableMagicFile' => 'bool',
            'magicFile' => 'string',
            'enableHeaderCheck' => 'bool',
            'mimeType' => 'string',
        ],
        'Zend\Validator\File\Md5' => [
            'algorithm' => 'string',
            'hash' => 'string',
            'md5' => 'string',
        ],
        'Zend\Validator\File\MimeType' => [
            'disableMagicFile' => 'bool',
            'magicFile' => 'string',
            'enableHeaderCheck' => 'bool',
            'mimeType' => 'string',
        ],
        'Zend\Validator\File\NotExists' => [
            'directory' => 'string',
        ],
        'Zend\Validator\File\Sha1' => [
            'algorithm' => 'string',
            'hash' => 'string',
            'sha1' => 'string',
        ],
        'Zend\Validator\File\Size' => [
            'max' => 'int',
            'min' => 'int',
            'size' => 'int',
            'useByteString' => 'bool',
        ],
        'Zend\Validator\File\UploadFile' => [],
        'Zend\Validator\File\Upload' => [],
        'Zend\Validator\File\WordCount' => [
            'max' => 'int',
            'min' => 'int',
        ],
        'Zend\Validator\GreaterThan' => [
            'inclusive' => 'bool',
            'min' => 'int',
        ],
        'Zend\Validator\Hex' => [],
        'Zend\Validator\Hostname' => [
            'allow' => 'int',
            'useIdnCheck' => 'bool',
            'useTldCheck' => 'bool',
        ],
        'Zend\Validator\Iban' => [
            'country_code' => 'string',
            'allow_non_sepa' => 'bool',
        ],
        'Zend\Validator\Identical' => [
            'literal' => 'bool',
            'strict' => 'bool',
            'token' => 'string',
        ],
        'Zend\Validator\InArray' => [
            'strict' => 'bool',
            'recursive' => 'bool',
        ],
        'Zend\Validator\Ip' => [
            'allowipv4' => 'bool',
            'allowipv6' => 'bool',
            'allowipvfuture' => 'bool',
            'allowliteral' => 'bool',
        ],
        'Zend\Validator\Isbn' => [
            'type' => 'string',
            'separator' => 'string',
        ],
        'Zend\Validator\IsInstanceOf' => [
            'classname' => 'string',
        ],
        'Zend\Validator\LessThan' => [
            'inclusive' => 'bool',
            'max' => 'int',
        ],
        'Zend\Validator\NotEmpty' => [
            'type' => 'int',
        ],
        'Zend\Validator\Regex' => [
            'pattern' => 'string',
        ],
        'Zend\Validator\Sitemap\Changefreq' => [],
        'Zend\Validator\Sitemap\Lastmod' => [],
        'Zend\Validator\Sitemap\Loc' => [],
        'Zend\Validator\Sitemap\Priority' => [],
        'Zend\Validator\Step' => [
            'baseValue' => 'int|float',
            'step' => 'float',
        ],
        'Zend\Validator\StringLength' => [
            'max' => 'int',
            'min' => 'int',
            'encoding' => 'string',
        ],
        'Zend\Validator\Uri' => [
            'allowAbsolute' => 'bool',
            'allowRelative' => 'bool',
        ],
        'Zend\Validator\Uuid' => [],
        'Zend\I18n\Validator\Alnum' => [
            'allowwhitespace' => 'bool',
        ],
        'Zend\I18n\Validator\Alpha' => [
            'allowwhitespace' => 'bool',
        ],
        'Zend\I18n\Validator\DateTime' => [
            'calendar' => 'int',
            'datetype' => 'int',
            'pattern' => 'string',
            'timetype' => 'int',
            'timezone' => 'string',
            'locale' => 'string',
        ],
        'Zend\I18n\Validator\Float' => [
            'locale' => 'string',
        ],
        'Zend\I18n\Validator\Int' => [
            'locale' => 'string',
        ],
        'Zend\I18n\Validator\IsFloat' => [
            'locale' => 'string',
        ],
        'Zend\I18n\Validator\IsInt' => [
            'locale' => 'string',
        ],
        'Zend\I18n\Validator\PhoneNumber' => [
            'country' => 'string',
            'allow_possible' => 'bool',
        ],
        'Zend\I18n\Validator\PostCode' => [
            'locale' => 'string',
            'format' => 'string',
            'service' => 'string',
        ],
    ];

    /**
     * @return string
     */
    public function getAll()
    {
        return $this->data['__all__'];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }
}
