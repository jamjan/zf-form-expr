<?php

namespace FormGenerator\Form;

use Zend\Form\Form as Form;
use FormGenerator\Entity\FormCreateEntity as FormCreateEntity;
use Zend\InputFilter\InputFilter as InputFilter;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class CreateForm extends Form
{
    public function __construct()
    {
        parent::__construct('form');

        $this
            ->setAttribute('method', 'post')
            ->setAttribute('id', 'formCreate')
            ->setAttribute('class', 'form-collection')
            ->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new FormCreateEntity())
            ->setInputFilter(new InputFilter())
        ;

        $this->add([
            'type' => 'FormGenerator\Form\FormFieldset',
            'options' => [
                'use_as_base_fieldset' => true,
                'label' => 'Create Form',
            ],
            'attributes' => [
                'class' => 'form-item',
            ],
        ],['priority'=>10]);

        $this->add([
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
        ],['priority'=>20]);

        $this->add([
            'name' => 'submit',
            'attributes' => [
                'type' => 'submit',
                'value' => 'Send',
                'class' => 'btn btn-success',
            ],
        ],['priority'=>30]);
    }
}
