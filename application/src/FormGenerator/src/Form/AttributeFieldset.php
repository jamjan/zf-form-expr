<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\AttributeEntity as AttributeEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class AttributeFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('element');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new AttributeEntity())
            ->setAttribute('class', 'attribute-item')
        ;

        $this->setLabel('Attribute');

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => 'Attribute Name',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(required)',
            ],
        ]);

        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Attribute Value',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_attribute',
            'options' => [
                'label' => 'Remove Attribute',
            ],
            'attributes' => [
                'id' => 'remove-attribute',
                'class' => 'btn btn-xs btn-warning',
                'onClick' => 'javascript:formCreate.remove($(this),"attribute");return false;',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => true,
            ],
            'value' => [
                'required' => false,
            ],
        ];
    }
}
