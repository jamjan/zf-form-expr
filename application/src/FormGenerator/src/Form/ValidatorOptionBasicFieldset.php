<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\AttributeEntity as AttributeEntity;
use FormGenerator\Entity\ValidatorMetadataEntity as ValidatorMetadataEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class ValidatorOptionBasicFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('validator');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new AttributeEntity())
            ->setAttribute('class', 'validator_option-item')
        ;

        $this->setLabel('Validator Option');

        $entity = new ValidatorMetadataEntity();

        foreach($entity->getAll() as $name => $placeholder){
            $this->add([
                'name' => $name,
                'options' => [
                    'label' => $name,
                    'label_attributes' => [
                        'class' => 'control-label',
                    ],
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'placeholder' => $placeholder,
                ],
            ]);
        }
    }

    public function getInputFilterSpecification()
    {
        return [];
    }
}
