<?php
namespace FormGenerator\Entity;

class FormOutputEntity
{
    /**
     * @var string
     */
    protected $method;

    protected $output_format;

    protected $output_fw;

    protected $output_generate;

    /**
     * @return mixed
     */
    public function getOutputFormat()
    {
        return $this->output_format;
    }

    public function setOutputFormat($output_format)
    {
        $this->output_format = $output_format;
        return $this;
    }

    public function getOutputFw()
    {
        return $this->output_fw;
    }

    public function setOutputFw($output_fw)
    {
        $this->output_fw = $output_fw;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutputGenerate()
    {
        return $this->output_generate;
    }

    public function setOutputGenerate($output_generate)
    {
        $this->output_generate = $output_generate;

        return $this;
    }
}
