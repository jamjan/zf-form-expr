<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\ElementSelectEntity as Entity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class ElementSelectFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('element_select');

        $this->setHydrator(new ClassMethodsHydrator(true))
            ->setObject(new Entity())
            ->setAttribute('class', 'element_select-collection')
        ;

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'add_element_select_value_options',
            'options' => [
                'label' => 'Add New Select Option',
            ],
            'attributes' => [
                'id' => 'add-element_select_value_options',
                'class' => 'btn btn-xs btn-primary btn-element_select_value_options',
                'onClick' => 'javascript:formCreate.add($(this),"element_select_value_options",null,true);return false;',
            ],
        ]);

        $this->add([
            'type' => 'FormGenerator\Form\ElementSelectBasicFieldset',
            'name' => 'basic',
            'attributes' => [
                'class' => 'element_select_basic-collection container-element_select_basic-collection',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'value_options',
            'options' => [
                'label' => 'Value Options',
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__index_form_element_select_value_options__',
                'target_element' => [
                    'type' => 'FormGenerator\Form\ElementSelectValueOptionsFieldset',
                ],
            ],
            'attributes' => [
                'class' => 'element_select_value_options-collection container-element_select_value_options-collection container-collection hidden',
                'data-template-index-placeholder' => '__index_form_element_select_value_options__',
            ],
        ]);

    }

    public function getInputFilterSpecification()
    {
        return [];
    }
}
