<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\ElementCheckboxValueOptionsEntity as Entity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;
use FormGenerator\Hydrator\InputNameStrategy;

class ElementCheckboxValueOptionsFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('element_checkbox_value_options');

        $hydrator = new ClassMethodsHydrator(true);
        $hydrator->addStrategy('value', new InputNameStrategy());

        $this->setHydrator($hydrator)
            ->setObject(new Entity())
            ->setAttribute('class', 'element_checkbox_value_options-item')
        ;

        $this->setLabel('Option');

        $this->add([
            'name' => 'label',
            'options' => [
                'label' => 'Option Label',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(required)',
            ],
        ]);
        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Value',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_element_checkbox_value_options',
            'options' => [
                'label' => 'Remove Checkbox Value Option',
            ],
            'attributes' => [
                'id' => 'remove-element_checkbox_value_options',
                'class' => 'btn btn-xs btn-danger pull-right',
                'onClick' => 'javascript:formCreate.remove($(this),"element_checkbox_value_options");return false;',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'label' => [
                'required' => true,
            ],
            'value' => [
                'required' => false,
            ],
        ];
    }
}
