<?php

namespace FormGenerator;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
            'application'    => [
                'form.create.display' => [
                    'forms' => [
                        'create_form'=>[
                            'form' => [
                                'name' => "create_form",
                                'class' => \FormGenerator\Form\CreateForm::class,
                                'entity_class' => \FormGenerator\Entity\FormCreateEntity::class,
                            ],


                        ],
                    ],
                ],
                'form.create.process' => [
                    'forms' => [
                        'create_form'=>[
                            'form' => [
                                'name' => "create_form",
                                'class' => \FormGenerator\Form\CreateForm::class,
                                'entity_class' => \FormGenerator\Entity\FormCreateEntity::class,
                            ],

                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'invokables' => [],
            'factories'  => [
                Action\CreateFormAction::class => Action\CreateFormFactory::class,
                Action\ProcessFormAction::class => Action\ProcessFormActionFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'form_generator'    => [__DIR__ . '/../templates/app'],
            ],
        ];
    }
}
