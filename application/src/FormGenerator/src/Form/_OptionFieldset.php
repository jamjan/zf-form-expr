<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\OptionEntity as OptionEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class OptionFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('element');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new OptionEntity())
        ;

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => 'Name',
            ],
        ]);

        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Value',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_option',
            'options' => [
                'label' => 'Remove Option',
            ],
            'attributes' => [
                'id' => 'remove-option',
                'class' => 'btn btn-sm',
                'onClick' => 'javascript:remove_option($(this));',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => true,
            ],
            'value' => [
                'required' => true,
            ],
        ];
    }
}
