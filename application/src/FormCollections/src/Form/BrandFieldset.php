<?php

namespace FormCollections\Form;

use FormCollections\Entity\Brand;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class BrandFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('brand');

        $this
            ->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new Brand())
        ;

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => 'Name of the brand',
            ],
            'attributes' => [
                'required' => 'required',
            ],
        ]);

        $this->add([
            'name' => 'url',
            'type' => 'Zend\Form\Element\Url',
            'options' => [
                'label' => 'Website of the brand',
            ],
            'attributes' => [
                'required' => 'required',
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => true,
            ],
        ];
    }
}
