<?php
namespace FormGenerator\Entity;

class LabelEntity
{
    /**
     * @var string
     */
    protected $value;

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return LabelEntity
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public function toArray()
    {
        return [
            'value' => $this->getValue(),
        ];
    }
}
