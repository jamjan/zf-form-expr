<?php

namespace FormGenerator\Action;

use FormGenerator\Form\CreateForm as CreateForm;
use FormGenerator\Entity\FormEntity as FormEntity;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Form\Factory as FormFactory;

class ProcessFormActionFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $router   = $container->get(RouterInterface::class);
        $template = $container->has(TemplateRendererInterface::class)
            ? $container->get(TemplateRendererInterface::class)
            : null;

        $requestedClass = new ProcessFormAction($router, $template);

        return $requestedClass;
    }
}
