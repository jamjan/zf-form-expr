<?php
return [
    'forms' => [
        'form_generator' => [
            0 => [
                'alias'=>'create'
            ],
        ],
    ],
    'application' => [
        'form.create.display' => [
            'forms' => [
                'form_create'=>[
                    'form' => [
                        'name' => "form_create",
                        'class' => \FormGenerator\Form\CreateForm::class,
                    ],
                    'entity' => [
                        'class' => \FormGenerator\Entity\FormCreateEntity::class,
                    ],

                ],
            ],
        ],
    ],
];
