<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\AttributeEntity as AttributeEntity;
use FormGenerator\Entity\ValidatorMetadataEntity as ValidatorMetadataEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class ValidatorOptionFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('validator_option');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new AttributeEntity())
            ->setAttribute('class', 'validator_option-item')
        ;

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => 'Name',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(string)',
            ],
        ]);
        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Value',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(string)',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => true,
            ],
            'value' => [
                'required' => false,
            ],
        ];
    }
}
