<?php

namespace FormGenerator\Entity;

class ElementSelectBasicEntity
{

    protected $disableInArrayValidator = false;

    protected $emptyOption;

    protected $useHiddenElement = false;

    protected $unselectedValue = '';

    /**
     * @return bool
     */
    public function isDisableInArrayValidator()
    {
        return $this->disableInArrayValidator;
    }

    /**
     * @param bool $disableInArrayValidator
     * @return ElementSelectBasicEntity
     */
    public function setDisableInArrayValidator($disableInArrayValidator)
    {
        $this->disableInArrayValidator = $disableInArrayValidator;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmptyOption()
    {
        return $this->emptyOption;
    }

    /**
     * @param mixed $emptyOption
     * @return ElementSelectBasicEntity
     */
    public function setEmptyOption($emptyOption)
    {
        $this->emptyOption = $emptyOption;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUseHiddenElement()
    {
        return $this->useHiddenElement;
    }

    /**
     * @param bool $useHiddenElement
     * @return ElementSelectBasicEntity
     */
    public function setUseHiddenElement($useHiddenElement)
    {
        $this->useHiddenElement = $useHiddenElement;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnselectedValue()
    {
        return $this->unselectedValue;
    }

    /**
     * @param string $unselectedValue
     * @return ElementSelectBasicEntity
     */
    public function setUnselectedValue($unselectedValue)
    {
        $this->unselectedValue = $unselectedValue;
        return $this;
    }

    public function toArray()
    {
        $data['disableInArrayValidator'] = $this->isDisableInArrayValidator();
        $data['emptyOption'] = $this->getEmptyOption();
        $data['useHiddenElement'] = $this->getUnselectedValue();
        $data['unselectedValue'] = $this->getUnselectedValue();
        return $data;
    }
}
