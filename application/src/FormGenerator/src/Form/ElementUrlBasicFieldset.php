<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\ElementUrlBasicEntity as Entity;
use FormGenerator\Hydrator\InputNameStrategy;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class ElementUrlBasicFieldset extends Fieldset implements InputFilterProviderInterface
{

    public function __construct()
    {
        parent::__construct('element_url_basic');

        $hydrator = new ClassMethodsHydrator(true);
        $hydrator->addStrategy('name', new InputNameStrategy());

        $this->setHydrator($hydrator)
            ->setObject(new Entity())
            ->setAttribute('class', 'element_url_basic-item')
        ;

        $this->setLabel('Element Specific Settings');

        $this->add([
            'name' => 'allow_absolute',
            'options' => [
                'label' => 'Allow Absolute',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(boolean)',
            ],
        ]);
        $this->add([
            'name' => 'allow_relative',
            'options' => [
                'label' => 'Allow relative',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(boolean)',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'empty_option' => [
                'required' => false,
            ],
            'use_hidden_element' => [
                'required' => false,
            ],
            'unselected_value' => [
                'required' => false,
            ],
            'disable_inarray_validator' => [
                'required' => false,
            ],
        ];
    }
}
