<?php
return [
    'service_manager' => [
        'abstract_factories' => [],
        'aliases'            => [],
        'delegators'         => [],
        'factories'          => [
            'FormElementManager' => "Zend\\Form\\FormElementManagerFactory",
            'InputFilterManager' => "Zend\\InputFilter\\InputFilterPluginManagerFactory",
        ],
        'initializers'       => [],
        'invokables'         => [],
        'lazy_services'      => [],
        'services'           => [],
        'shared'             => [],
    ],
];
