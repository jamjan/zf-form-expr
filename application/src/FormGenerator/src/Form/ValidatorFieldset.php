<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\AttributeEntity as AttributeEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class ValidatorFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('validator');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new AttributeEntity())
            ->setAttribute('class', 'validator-item')
        ;

        $this->setLabel('Validator');

        $this->add([
            'type' => 'Zend\Form\Element\Select',
            'name' => 'value',
            'options' => [
                'label' => 'Validator',
                'empty_option' => '--validator--',
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);


        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_validator',
            'options' => [
                'label' => 'Remove Validator',
            ],
            'attributes' => [
                'id' => 'remove-validator',
                'class' => 'btn btn-xs btn-danger pull-right',
                'onClick' => 'javascript:formCreate.remove($(this),"validator");return false;',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'add_validator_option',
            'options' => [
                'label' => 'New Validator Option',
            ],
            'attributes' => [
                'id' => 'add-add_validator_option',
                'class' => 'btn btn-xs btn-primary',
//                'onClick' => 'javascript:formCreate.addElementValidatorOptionFieldset($(this));formCreate.getValidatorOptions($(this),formCreate.test());return false;',
                'onClick' => 'javascript:formCreate.add($(this),"validator_option");return false;',
            ],
        ]);
        $this->add([
            'type' => 'FormGenerator\Form\ValidatorOptionBasicFieldset',
            'name' => 'validator_option_basic',
            'options' => [
                'label' => 'Validator Basic Options',
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__index_form_element_validator_option_basic__',
            ],
            'attributes' => [
                'class' => 'validator_option_basic-collection container-validator_option_basic-collection',
                'data-template-index-placeholder' => '__index_form_element_validator_option_basic__',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'validator_option',
            'options' => [
                'label' => 'Validator Specific Options',
                'count' => 1,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__index_form_element_validator_option__',
                'target_element' => [
                    'type' => 'FormGenerator\Form\ValidatorOptionFieldset',
                ],
            ],
            'attributes' => [
                'class' => 'validator_option-collection container-validator_option-collection',
                'data-template-index-placeholder' => '__index_form_element_validator_option__',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'value' => [
                'required' => false,
            ],
        ];
    }
}
