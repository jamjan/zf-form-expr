<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\FilterOptionEntity as FilterOptionEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class FilterOptionFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('filter_option');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new FilterOptionEntity())
            ->setAttribute('class', 'filter_option-item')
        ;

        $this->setLabel('Filter Option');

/*
        $entity = new ValidatorMetadataEntity();

        foreach($entity->getAll() as $name => $placeholder){
            $this->add([
                'name' => $name,
                'options' => [
                    'label' => $name,
                    'label_attributes' => [
                        'class' => 'control-label',
                    ],
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'placeholder' => $placeholder,
                ],
            ]);
        }
*/
        $this->add([
            'type' => 'Zend\Form\Element\Select',
            'name' => 'name',
            'options' => [
                'label' => 'Name',
                'empty_option' => '--validator--',
                'value_options' => [
                    '0' => 'String Length',
                    '1' => 'Alnum',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Value',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_filter_option',
            'options' => [
                'label' => 'Remove Filter Option',
            ],
            'attributes' => [
                'id' => 'remove-filter_option',
                'class' => 'btn btn-xs btn-danger pull-right',
                'onClick' => 'javascript:formCreate.remove($(this),"filter_option");return false;',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => true,
            ],
            'value' => [
                'required' => false,
            ],
        ];
    }
}
