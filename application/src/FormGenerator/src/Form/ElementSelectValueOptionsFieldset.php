<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\ElementSelectValueOptionsEntity as Entity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;
use FormGenerator\Hydrator\InputNameStrategy;

class ElementSelectValueOptionsFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('value_options');

        $hydrator = new ClassMethodsHydrator(true);
        $hydrator->addStrategy('value', new InputNameStrategy());

        $this->setHydrator($hydrator)
            ->setObject(new Entity())
            ->setAttribute('class', 'element_select_value_options-item container-item')
        ;

        $this->setLabel('Option');

        $this->add([
            'name' => 'label',
            'options' => [
                'label' => 'Option Label',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(required)',
            ],
        ]);
        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Value',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_element_select_value_options',
            'options' => [
                'label' => 'Remove Select Value Option',
            ],
            'attributes' => [
                'id' => 'remove-element_select_value_options',
                'class' => 'btn btn-xs btn-danger pull-right',
                'onClick' => 'javascript:formCreate.remove($(this),"element_select_value_options");return false;',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'label' => [
                'required' => true,
            ],
            'value' => [
                'required' => false,
            ],
        ];
    }
}
