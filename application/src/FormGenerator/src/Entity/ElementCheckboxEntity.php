<?php

namespace FormGenerator\Entity;

class ElementCheckboxEntity
{

    /**
     * @var ElementCheckboxBasicEntity|null
     */
    protected $basic;

    /**
     * @var ElementCheckboxValueOptionsEntity|null
     */
    protected $value_options;

    /**
     * @return ElementCheckboxBasicEntity|null
     */
    public function getBasic()
    {
        return $this->basic;
    }

    /**
     * @param ElementCheckboxBasicEntity|null $basic
     * @return ElementCheckboxEntity
     */
    public function setBasic(ElementCheckboxBasicEntity $basic)
    {
        $this->basic = $basic;
        return $this;
    }

    /**
     * @return ElementCheckboxValueOptionsEntity|null
     */
    public function getValueOptions()
    {
        return $this->value_options;
    }

    /**
     * @param ElementCheckboxValueOptionsEntity|null $value_options
     * @return ElementCheckboxEntity
     */
    public function setValueOptions($value_options)
    {
        $this->value_options = $value_options;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data['basic'] = $this->getBasic();
        $data['value_options'] = $this->getValueOptions();
        return $data;
    }
}
