<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\RadioOptionsEntity as RadioOptionsEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class RadioOptionsFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('element_radio_options');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new RadioOptionsEntity())
            ->setAttribute('class', 'element_radio_options-item')
        ;

//        $this->setLabel('Label');

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => 'Name',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);
        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Value',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(required)',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_element_radio_options',
            'options' => [
                'label' => 'Remove Radio Option',
            ],
            'attributes' => [
                'id' => 'remove-element_radio_options',
                'class' => 'btn btn-xs btn-danger pull-right',
                'onClick' => 'javascript:formCreate.remove($(this),"element_radio_options");return false;',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => false,
            ],
            'value' => [
                'required' => true,
            ],
        ];
    }
}
