<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\ElementEntity as ElementEntity;
use FormGenerator\Hydrator\InputNameStrategy;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class ElementFieldset extends Fieldset implements InputFilterProviderInterface
{

    public function __construct()
    {
        parent::__construct('element');

        $hydrator = new ClassMethodsHydrator(true);
        $hydrator->addStrategy('name', new InputNameStrategy());

        $this->setHydrator($hydrator)
            ->setObject(new ElementEntity())
            ->setAttribute('class', 'element-item')
        ;

        $this->setLabel('Element');

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_element',
            'options' => [
                'label' => 'Remove Element',
            ],
            'attributes' => [
                'id' => 'remove-element',
                'class' => 'btn btn-xs btn-danger float-right',
                'onClick' => 'javascript:formCreate.remove($(this),"element");reindexAllElementsAttributeName($(this));return false;',
            ],
        ]);

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => 'Name',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Select',
            'name' => 'type',
            'options' => [
                'label' => 'Type',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
                'value_options' => [
                    'text' => 'Text',
                    'textarea' => 'Textarea',
                    'email' => 'Email',
                    'url' => 'Url',
                    'select' => 'Select',
                    'checkbox' => 'Checkbox',
                    'radio' => 'Radio',
                    'password' => 'Password',
                    'hidden' => 'Hidden',
                ],
            ],
            'attributes' => [
                'class' => 'form-control toggle-aware-trigger',
                'data-toggle' => '{"select":{"show":["element_select"]},"checkbox":{"show":["element_checkbox"]},"radio":{"show":["element_radio"]},"url":{"show":["element_url"]}}',
                'onChange' => 'javascript:formCreate.toggleFieldset($(this));return false;',
            ],
        ]);

        $this->add([
            'type' => 'FormGenerator\Form\LabelFieldset',
            'name' => 'label',
        ]);

        // Select Element:
        $this->add([
            'type' => 'FormGenerator\Form\ElementSelectFieldset',
            'name' => 'element_select',
            'options' => [
                'label' => 'Type Select',
            ],
            'attributes' => [
                'class' => 'element_select-collection hidden',
            ],
        ]);

        // Checkbox Element:
        $this->add([
            'type' => 'FormGenerator\Form\ElementCheckboxFieldset',
            'name' => 'element_checkbox',
            'options' => [
                'label' => 'Type Checkbox',
            ],
            'attributes' => [
                'class' => 'element_checkbox-collection hidden',
            ],
        ]);

        // Radio Element:
        $this->add([
            'type' => 'FormGenerator\Form\ElementRadioFieldset',
            'name' => 'element_radio',
            'options' => [
                'label' => 'Type Radio',
            ],
            'attributes' => [
                'class' => 'element_radio-collection hidden',
            ],
        ]);

        // Radio Element:
        $this->add([
            'type' => 'FormGenerator\Form\ElementUrlFieldset',
            'name' => 'element_url',
            'options' => [
                'label' => 'Type URL',
            ],
            'attributes' => [
                'class' => 'element_url-collection hidden',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'add_attribute',
            'options' => [
                'label' => 'New Element Attribute',
            ],
            'attributes' => [
                'id' => 'add-attribute',
                'class' => 'btn btn-xs btn-primary',
                'onClick' => 'javascript:formCreate.add($(this),"attribute");return false;',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'attributes',
            'options' => [
                'label' => 'Attributes',
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__index_form_element_attribute__',
                'target_element' => [
                    'type' => 'FormGenerator\Form\AttributeFieldset',
                ],
            ],
            'attributes' => [
                'class' => 'attribute-collection container-attribute-collection',
                'data-template-index-placeholder' => '__index_form_element_attribute__',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'add_option',
            'options' => [
                'label' => 'New Option',
            ],
            'attributes' => [
                'id' => 'add-option',
                'class' => 'btn btn-xs btn-primary',
                'onClick' => 'javascript:formCreate.add($(this),"option");return false;',
            ],
        ]);
        $this->add([
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'options',
            'options' => [
                'label' => 'Options',
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__index_form_element_option__',
                'target_element' => [
                    'type' => 'FormGenerator\Form\OptionFieldset',
                ],
            ],
            'attributes' => [
                'class' => 'option-collection container-option-collection',
                'data-template-index-placeholder' => '__index_form_element_option__',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'add_label_attribute',
            'options' => [
                'label' => 'New Label Attribute',
            ],
            'attributes' => [
                'id' => 'add-label_attribute',
                'class' => 'btn btn-xs btn-primary',
                'onClick' => 'javascript:formCreate.add($(this),"label_attribute");return false;',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'label_attributes',
            'options' => [
                'label' => 'Label Attributes',
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__index_form_element_label_attribute__',
                'target_element' => [
                    'type' => 'FormGenerator\Form\LabelAttributeFieldset',
                ],
            ],
            'attributes' => [
                'class' => 'label_attribute-collection container-label_attribute-collection',
                'data-template-index-placeholder' => '__index_form_element_label_attribute__',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'add_label_option',
            'options' => [
                'label' => 'New Label Option',
            ],
            'attributes' => [
                'id' => 'add-label_option',
                'class' => 'btn btn-xs btn-primary',
                'onClick' => 'javascript:formCreate.add($(this),"label_option");return false;',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'label_options',
            'options' => [
                'label' => 'Label Options',
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__index_form_element_label_option__',
                'target_element' => [
                    'type' => 'FormGenerator\Form\LabelOptionFieldset',
                ],
            ],
            'attributes' => [
                'class' => 'label_option-collection container-label_option-collection',
                'data-template-index-placeholder' => '__index_form_element_label_option__',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'add_filter',
            'options' => [
                'label' => 'New Filter',
            ],
            'attributes' => [
                'id' => 'add-filter',
                'class' => 'btn btn-xs btn-primary','onClick' => 'javascript:formCreate.add($(this),"filter");return false;',
            ],
        ]);
        $this->add([
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'filter',
            'options' => [
                'label' => 'Filters',
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__index_form_element_filter__',
                'target_element' => [
                    'type' => 'FormGenerator\Form\FilterFieldset',
                ],
            ],
            'attributes' => [
                'class' => 'filter-collection container-filter-collection',
                'data-template-index-placeholder' => '__index_form_element_filter__',

            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'add_validator',
            'options' => [
                'label' => 'New Validator',
            ],
            'attributes' => [
                'id' => 'add-validator',
                'class' => 'btn btn-xs btn-primary',
                'onClick' => 'javascript:formCreate.add($(this),"validator");return false;',
            ],
        ]);
        $this->add([
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'validator',
            'options' => [
                'label' => 'Validators',
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__index_form_element_validator__',
                'target_element' => [
                    'type' => 'FormGenerator\Form\ValidatorFieldset',
                ],
            ],
            'attributes' => [
                'class' => 'validator-collection container-validator-collection',
                'data-template-index-placeholder' => '__index_form_element_validator__',
                'data-ajax-aware' => true,
                'data-ajax-type' => 'validator',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => false,
            ],
            'type' => [
                'required' => true,
            ],
        ];
    }
}
