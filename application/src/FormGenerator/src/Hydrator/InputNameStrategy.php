<?php
namespace FormGenerator\Hydrator;

use DateTime;
use Ramsey\Uuid\Uuid as Uuid;
use Zend\Hydrator\Strategy\DefaultStrategy;

class InputNameStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     *
     * Convert a string value into a underscore
     * #TODO description
     */
    public function hydrate($value)
    {
        if (empty($value)) {
            $value = str_replace('-', '_', Uuid::uuid4());
        }

        return $value;
    }
}
