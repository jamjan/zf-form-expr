<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\FilterEntity as FilterEntity;
use FormGenerator\Entity\FilterMetadataEntity as FilterMetadataEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class FilterFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('filter');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new FilterEntity())
            ->setAttribute('class', 'filter-item')
        ;

        $this->setLabel('Filter');
/*
        $entity = new ValidatorMetadataEntity();

        foreach($entity->getAll() as $name => $placeholder){
            $this->add([
                'name' => $name,
                'options' => [
                    'label' => $name,
                    'label_attributes' => [
                        'class' => 'control-label',
                    ],
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'placeholder' => $placeholder,
                ],
            ]);
        }
*/

        $entity = new FilterMetadataEntity();
        $filters=[];
        foreach($entity->toArray() as $name => $settings){
            $nameNormalized = str_replace('\\','_',$name);
            $filters[$nameNormalized]=$name;
        }

        $this->add([
            'type' => 'Zend\Form\Element\Select',
            'name' => 'name',
            'options' => [
                'label' => 'Filter',
                'empty_option' => '--filters--',
                'value_options' => $filters,
            ],
            'attributes' => [
                'class' => 'form-control',
                'data-toggle' => '',
            ],
        ]);

        foreach($entity->toArray() as $name => $settings){
            $nameNormalized = str_replace('\\','_',$name);
            $filters[$nameNormalized]=$name;
            if(!empty($settings)) {
                foreach($settings as $settingName=>$settingType){
                    $this->add([
                        'name' => $settingName,
                        'options' => [
                            'label' => $settingName,
                            'label_attributes' => [
                                'class' => 'control-label',
                            ],
                        ],
                        'attributes' => [
                            'class' => 'form-control',
                            'placeholder' => $settingType,
                        ],
                    ]);
                }


            }
        }


        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_filter',
            'options' => [
                'label' => 'Remove Filter',
            ],
            'attributes' => [
                'id' => 'remove-filter',
                'class' => 'btn btn-xs btn-danger pull-right',
                'onClick' => 'javascript:formCreate.remove($(this),"filter");return false;',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'filter_option',
            'options' => [
                'label' => 'Filter Specific Options',
                'count' => count($filters),
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__index_form_element_filter_option_filter__',
                'target_element' => [
                    'type' => 'FormGenerator\Form\FilterOptionFieldset',
                ],
            ],
            'attributes' => [
                'class' => 'filter_option-collection container-filter_option-collection',
                'data-template-index-placeholder' => '__index_form_element_filter_option_filter__',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => true,
            ],
        ];
    }
}
