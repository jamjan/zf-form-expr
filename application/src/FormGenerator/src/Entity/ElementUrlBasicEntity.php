<?php

namespace FormGenerator\Entity;

class ElementUrlBasicEntity
{
    protected $allowAbsolute = true;

    protected $allowRelative = false;

    /**
     * @return bool
     */
    public function isAllowAbsolute()
    {
        return $this->allowAbsolute;
    }

    /**
     * @param bool $allowAbsolute
     * @return ElementUrlBasicEntity
     */
    public function setAllowAbsolute($allowAbsolute)
    {
        $this->allowAbsolute = $allowAbsolute;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowRelative()
    {
        return $this->allowRelative;
    }

    /**
     * @param bool $allowRelative
     * @return ElementUrlBasicEntity
     */
    public function setAllowRelative($allowRelative)
    {
        $this->allowRelative = $allowRelative;
        return $this;
    }

    public function toArray()
    {
        $data['allowAbsolute'] = $this->isDisableInArrayValidator();
        $data['allowRelative'] = $this->getEmptyOption();
        return $data;
    }
}
