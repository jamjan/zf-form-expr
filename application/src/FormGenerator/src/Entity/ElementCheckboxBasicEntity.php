<?php

namespace FormGenerator\Entity;

class ElementCheckboxBasicEntity
{

    /**
     * @var bool
     */
    protected $useHiddenElement = true;

    /**
     * @var string
     */
    protected $uncheckedValue = '0';

    /**
     * @var string
     */
    protected $checkedValue = '1';

    /**
     * @return bool
     */
    public function isUseHiddenElement()
    {
        return $this->useHiddenElement;
    }

    /**
     * @param bool $useHiddenElement
     * @return ElementCheckboxBasicEntity
     */
    public function setUseHiddenElement($useHiddenElement)
    {
        $this->useHiddenElement = $useHiddenElement;
        return $this;
    }

    /**
     * @return string
     */
    public function getUncheckedValue()
    {
        return $this->uncheckedValue;
    }

    /**
     * @param string $uncheckedValue
     * @return ElementCheckboxBasicEntity
     */
    public function setUncheckedValue($uncheckedValue)
    {
        $this->uncheckedValue = $uncheckedValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getCheckedValue()
    {
        return $this->checkedValue;
    }

    /**
     * @param string $checkedValue
     * @return ElementCheckboxBasicEntity
     */
    public function setCheckedValue($checkedValue)
    {
        $this->checkedValue = $checkedValue;
        return $this;
    }

    public function toArray()
    {
        $data['useHiddenElement'] = (int) $this->isUseHiddenElement();
        $data['uncheckedValue'] = $this->getUncheckedValue();
        $data['checkedValue'] = $this->getCheckedValue();
        return $data;
    }
}
