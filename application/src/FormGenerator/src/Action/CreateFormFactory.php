<?php

namespace FormGenerator\Action;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class CreateFormFactory
{
    public function __invoke(ContainerInterface $container)
    {
        // @todo that could be extracted to the parent?
        $router   = $container->get(RouterInterface::class);
        $template = $container->has(TemplateRendererInterface::class)
            ? $container->get(TemplateRendererInterface::class)
            : null;

        $requestedClass = new CreateFormAction($router, $template);

        return $requestedClass;
    }

    public static function __callStatic($name, $arguments)
    {
        // TODO: Implement __callStatic() method.
    }
}
