<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\ElementRadioBasicEntity as Entity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class ElementRadioBasicFieldset extends Fieldset implements InputFilterProviderInterface
{

    public function __construct()
    {
        parent::__construct('element_radio_basic');

        $hydrator = new ClassMethodsHydrator(true);

        $this->setHydrator($hydrator)
            ->setObject(new Entity())
            ->setAttribute('class', 'element_radio_basic-item')
        ;

        $this->setLabel('Specific Settings');

        $this->add([
            'name' => 'disable_in_array_validator',
            'options' => [
                'label' => 'Disable inArray validator',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(boolean)',
            ],
        ]);

        $this->add([
            'name' => 'use_hidden_element',
            'options' => [
                'label' => 'Use Hidden Element',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(boolean)',
            ],
        ]);

        $this->add([
            'name' => 'unselected_value',
            'options' => [
                'label' => 'Unselected Value',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(string)',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'disable_in_array_validator' => [
                'required' => false,
            ],
            'use_hidden_element' => [
                'required' => false,
            ],
            'unselected_value' => [
                'required' => false,
            ],
        ];
    }
}
