<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\FormOutputEntity as FormOutputEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;
use FormGenerator\Hydrator\InputNameStrategy;

class FormOutputFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('basic');

        $hydrator = new ClassMethodsHydrator(true);
        $hydrator->addStrategy('name', new InputNameStrategy());

        $this->setHydrator($hydrator)
            ->setObject(new FormOutputEntity())
        ;

        $this->add([
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'output_format',
            'options' => [
                'label' => 'What is the expected output format?',
                'value_options' => [
                    [
                        'value' => 'class',
                        'label' => 'Class (PSR-4)',
                        'selected' => false,

                    ],
                    [
                        'value' => 'json',
                        'label' => 'JSON',
                        'selected' => true,

                    ],
                    [
                        'value' => 'annotation',
                        'label' => 'Annotation',
                        'selected' => false,

                    ],
                ],

            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'output_fw',
            'options' => [
                'label' => 'What CSS framework to use?',
                'value_options' => [
                    [
                        'value' => 'none',
                        'label' => 'None',
                        'selected' => false,

                    ],
                    [
                        'value' => 'bt3',
                        'label' => 'Bootstrap 3',
                        'selected' => true,

                    ],
                ],

            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'output_generate',
            'options' => [
                'label' => 'Do you want to generate new form for the preview?',
                'value_options' => [
                    [
                        'value' => '1',
                        'label' => 'Generate output form',
                        'selected' => true,

                    ],
                    [
                        'value' => '0',
                        'label' => 'No',
                        'selected' => false,

                    ],
                ],

            ],
        ]);

    }

    public function getInputFilterSpecification()
    {
        return [
            'output_format' => [
                'required' => false,
            ],
            'output_fw' => [
                'required' => false,
            ],
            'output_generate' => [
                'required' => false,
            ],
        ];
    }
}
