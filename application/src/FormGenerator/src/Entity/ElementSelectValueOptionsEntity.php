<?php
namespace FormGenerator\Entity;

class ElementSelectValueOptionsEntity
{
    /**
     * @var string
     */
    protected $label;
    /**
     * @var string
     */
    protected $value;

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return ElementSelectValueOptionsEntity
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return ElementSelectValueOptionsEntity
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function toArray()
    {
        return [
            'value' => $this->getValue(),
            'label' => $this->getLabel(),
        ];
    }
}
