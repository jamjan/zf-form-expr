
function SecalithZfAjax(name){
    this.name = name;
    this.result = {};
    this.exeSuccess = function(r){
        window[this.name].setAjaxResult(r);
        return r;
    }
};

SecalithZfAjax.prototype.get = function(url) {
    return $.ajax({
        context: window[this.name],
        url: url,
        cache:true,
        async:false,
        success: this.exeSuccess
    });
}

