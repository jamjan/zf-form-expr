<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\CheckboxOptionsEntity as CheckboxOptionsEntity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class CheckboxOptionsFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('element_checkbox_options');

        $this->setHydrator(new ClassMethodsHydrator(false))
            ->setObject(new CheckboxOptionsEntity())
            ->setAttribute('class', 'element_checkbox_options-item')
        ;

//        $this->setLabel('Label');

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => 'Name',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(optional)',
            ],
        ]);
        $this->add([
            'name' => 'value',
            'options' => [
                'label' => 'Value',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(required)',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'remove_element_checkbox_options',
            'options' => [
                'label' => 'Remove Checkbox Option',
            ],
            'attributes' => [
                'id' => 'remove-element_checkbox_options',
                'class' => 'btn btn-xs btn-danger pull-right',
                'onClick' => 'javascript:formCreate.remove($(this),"element_checkbox_options");return false;',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => false,
            ],
            'value' => [
                'required' => true,
            ],
        ];
    }
}
