<?php
namespace App\Delegator;

use Zend\ServiceManager\DelegatorFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use App\Delegator\FormMiddleware;
use Zend\Expressive\Router\RouteResult;
use App\Common\FormAwareInterface;
use Zend\Expressive\Helper;

class FormEntityDelegatorFactory implements DelegatorFactoryInterface
{

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        if (! call_user_func($callback) instanceof FormAwareInterface) {
            return call_user_func($callback);
        }

        $config = $container->has('config') ? $container->get('config') : [];
        // Obtain current RouteNames
        $routeResult = $container->get(Helper\UrlHelper::class);
        $currentRouteName   = $routeResult->getMatchedRouteName();

        if (array_key_exists('application', $config)
            && array_key_exists($currentRouteName, $config['application'])) {
            // get the forms config by the routeName
            if (array_key_exists('forms', $config['application'][$currentRouteName])) {
                $formsDeclarationConfig = $config['application'][$currentRouteName]['forms'];
                if (! empty($formsDeclarationConfig)) {
                    foreach ($formsDeclarationConfig as $formName => $formDeclaration) {
                        if (is_string($formDeclaration['entity']['class'])) {
                            //$callback = call_user_func($callback)->addFormEntity(new $formDeclaration['entity']['class']());
                        }
                    }
                }
            }
        }

        if ($callback instanceof FormAwareInterface) {
            return $callback;
        }
//        $r = $container->get(FormMiddleware::class);
        return call_user_func($callback);
    }

    public function createDelegatorWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName, $callback)
    {
        $realBuzzer   = call_user_func($callback);
        $eventManager = $serviceLocator->get('EventManager');

        $eventManager->attach('buzz', function () {
            echo "Stare at the art!\n";
        });

        return new BuzzerDelegator($realBuzzer, $eventManager);
    }
}
