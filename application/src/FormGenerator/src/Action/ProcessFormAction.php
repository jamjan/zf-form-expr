<?php

namespace FormGenerator\Action;

use App\Common\FormAwareTrait as FormAwareTrait;
use App\Common\FormAwareInterface as FormAwareInterface;
use App\Common\FormFactoryAwareTrait as FormFactoryAwareTrait;
use App\Common\FormFactoryAwareInterface as FormFactoryAwareInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use FormGenerator\Entity\FormCreateEntity;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;
use Zend\Expressive\ZendView\ZendViewRenderer;
use Zend\Form\Element as Element;
use Zend\Form\Element\Checkbox as CheckboxElement;
use Zend\Form\Element\Select as SelectElement;
use Zend\Form\Element\Radio as RadioElement;

class ProcessFormAction implements ServerMiddlewareInterface, FormAwareInterface, FormFactoryAwareInterface
{

    use FormAwareTrait;
    use FormFactoryAwareTrait;

    private $router;

    private $template;

    public function __construct(Router\RouterInterface $router, Template\TemplateRendererInterface $template = null)
    {
        $this->router   = $router;
        $this->template = $template;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return HtmlResponse|JsonResponse
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $formPostData = $request->getParsedBody();
        $form = $this->getForm('create_form');
        $form->setData($formPostData);
        $this->addForm($form, 'create_form');

        $forJson = null;

        if (true===$this->getForm('create_form')->isValid()) {

            // collect parsed data
            $data = $this->getForm('create_form')->getData();
            // create empty form
            $newForm = $this->getFormFactory()->createForm([]);
            // set the id attribute
            $newForm->setAttribute('id', $data->getBasic()->getId());
            // set the name attribute for the form
            $newForm->setAttribute('name', $data->getBasic()->getName());
            // set the method forms attribute
            $newForm->setAttribute('method', $data->getBasic()->getMethod());

            $forJson = [];

            if (null!==$data->getElements()) {
                $i=0;
                /* @var \FormGenerator\Entity\ElementEntity $element */
                foreach ($data->getElements() as $element) {
                    if($element->getType()=='select'){
                        $newElement = new SelectElement($element->getName());
                        if( ! empty($element->getElementSelect()->getValueOptions())){
                            $valueOptions = [];
                            /* @var \FormGenerator\Entity\ElementSelectValueOptionsEntity $option */
                            foreach($element->getElementSelect()->getValueOptions() as $option){
                                $valueOptions[$option->getValue()] = $option->getLabel();
                            }
                            if($element->getElementSelect()->getBasic()->getEmptyOption()){
                                $emptyOption = $element->getElementSelect()->getBasic()->getEmptyOption();
                                $newElement->setEmptyOption($emptyOption);
                            }
                            $newElement->setValueOptions($valueOptions);
                        }
                    } elseif($element->getType()=='checkbox'){
                        $newElement = new CheckboxElement($element->getName());
                        if( ! empty($element->getElementCheckbox()->getValueOptions())){
                            $valueOptions = [];
                            /* @var \FormGenerator\Entity\ElementCheckboxValueOptionsEntity $option */
                            foreach($element->getElementCheckbox()->getValueOptions() as $option){
                                $valueOptions[$option->getValue()] = $option->getLabel();
                            }
                            $newElement->setValueOptions($valueOptions);
                        }
                    } elseif($element->getType()=='radio'){
                        $newElement = new RadioElement($element->getName());
                        if( ! empty($element->getElementRadio()->getValueOptions())){
                            $valueOptions = [];
                            /* @var \FormGenerator\Entity\ElementRadioValueOptionsEntity $option */
                            foreach($element->getElementRadio()->getValueOptions() as $option){
                                $valueOptions[$option->getValue()] = $option->getLabel();
                            }
                            $newElement->setValueOptions($valueOptions);
                        }
                    } else {
                        $newElement = new Element($element->getName());
                        $newElement->setAttributes([
                            'type'  => $element->getType()
                        ]);
                    }

                    $forJson['elements'][$i]['spec']['name'] = $element->getName();
                    $forJson['elements'][$i]['spec']['type'] = $element->getType();

                    if (null!==$element->getAttributes()) {
                        /* @var \FormGenerator\Entity\AttributeEntity $attribute */
                        foreach ($element->getAttributes() as $attribute) {
                            $newElement->setAttribute(
                                $attribute->getName(),
                                $attribute->getValue()
                            );
                            $forJson['elements'][$i]['spec']['attributes'][$attribute->getName()] = $attribute->getValue();
                        }
                    }

                    if (null!==$element->getLabel()) {
                        if (null!==$element->getLabel()->getValue()) {
                            $newElement->setLabel($element->getLabel()->getValue());
                            $forJson['elements'][$i]['spec']['options']['label'] = $element->getLabel()->getValue();
                        }
                        if(null!==$element->getLabelAttributes()){
                            $attrParsed = [];
                            foreach($element->getLabelAttributes() as $labelAttribute){
                                $attrParsed[$labelAttribute->getName()]=$labelAttribute->getValue();
                            }
                            $newElement->setLabelAttributes($attrParsed);
                        }
                        if(null!==$element->getLabelOptions()){
                            $attrParsed = [];
                            foreach($element->getLabelOptions() as $option){
                                $attrParsed[$option->getName()]=$option->getValue();
                            }
                            $newElement->setLabelOptions($attrParsed);
                        }

//                        if ('bt3'==$data->getOutput()->getOutputFw()) {
//                            $newElement->setLabelAttributes(['class'=>'control-label']);
//                        }
                    }

                    if (null!==$element->getOptions()) {
                        foreach ($element->getOptions() as $option) {
                            $newElement->setOption(
                                $option->getName(),
                                $option->getValue()
                            );
                            $forJson['elements'][$i]['spec']['options'][$option->getName()] = $option->getValue();
                        }
                    }

//                    if ('bt3'==$data->getOutput()->getOutputFw()) {
                        $newElement->setAttribute('class', $newElement->getAttribute('class').' form-control');
//                    }

                    // @todo validators

                    // @todo filters

                    $newForm->add($newElement);

                    $i++;
                }
            }

            $this->addForm($newForm, 'created');

            $tpl = 'form_generator::form-process';
        } else {
            // form is not valid.
            // @todo solve error-handling for the Forms
            $r = $form->getMessages();
            var_dump($r);
            $tpl = 'form_generator::form-create';
        }

        $this->addForm($form, $form->getAttribute('name'));

        if (! $this->template) {
            return new JsonResponse([
                'welcome' => 'Congratulations! You have installed the zend-expressive skeleton application.',
                'docsUrl' => 'https://docs.zendframework.com/zend-expressive/',
            ]);
        }

        $data = [
            'forms' => $this->getForms(),
            'json' => $forJson,
        ];

        return new HtmlResponse($this->template->render($tpl, $data));
    }
}
