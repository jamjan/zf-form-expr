<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\ElementCheckboxBasicEntity as Entity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class ElementCheckboxBasicFieldset extends Fieldset implements InputFilterProviderInterface
{

    public function __construct()
    {
        parent::__construct('element_checkbox_basic');

        $hydrator = new ClassMethodsHydrator(true);

        $this->setHydrator($hydrator)
            ->setObject(new Entity())
            ->setAttribute('class', 'element_checkbox_basic-item')
        ;

        $this->setLabel('Element Specific Settings');

        $this->add([
            'name' => 'use_hidden_element',
            'options' => [
                'label' => 'Use Hidden Element',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(boolean)',
            ],
        ]);

        $this->add([
            'name' => 'checked_value',
            'options' => [
                'label' => 'Checked Value',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(string)',
            ],
        ]);

        $this->add([
            'name' => 'unchecked_value',
            'options' => [
                'label' => 'Unchecked Value',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '(string)',
            ],
        ]);

        $this->add([
            'name' => 'is_checked',
            'options' => [
                'label' => 'Is Checked',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'use_hidden_element' => [
                'required' => false,
            ],
            'checked_value' => [
                'required' => false,
            ],
            'unchecked_value' => [
                'required' => false,
            ],
        ];
    }
}
