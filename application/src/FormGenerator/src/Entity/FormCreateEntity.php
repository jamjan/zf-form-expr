<?php
namespace FormGenerator\Entity;

class FormCreateEntity
{
    /**
     * @var array
     */
    protected $form;



    protected $output_format;

    protected $output_fw;

    protected $output_generate;
    /**
     * @var array
     */
    protected $element;

    /**
     * @return array
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param array $form
     * @return FormCreateEntity
     */
    public function setForm($form)
    {
        $this->form = $form;
        return $this;
    }

    /**
     * @return array
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * @param array $element
     * @return FormCreateEntity
     */
    public function setElement($element)
    {
        $this->element = $element;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getOutputFormat()
    {
        return $this->output_format;
    }

    /**
     * @param mixed $output_format
     * @return FormEntity
     */
    public function setOutputFormat($output_format)
    {
        $this->output_format = $output_format;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutputFw()
    {
        return $this->output_fw;
    }

    /**
     * @param mixed $output_fw
     * @return FormEntity
     */
    public function setOutputFw($output_fw)
    {
        $this->output_fw = $output_fw;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutputGenerate()
    {
        return $this->output_generate;
    }

    /**
     * @param mixed $output_generate
     * @return FormEntity
     */
    public function setOutputGenerate($output_generate)
    {
        $this->output_generate = $output_generate;
        return $this;
    }
}
