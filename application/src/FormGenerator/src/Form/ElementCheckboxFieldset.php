<?php
namespace FormGenerator\Form;

use FormGenerator\Entity\ElementCheckboxEntity as Entity;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;

class ElementCheckboxFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('element_checkbox');

        $this->setHydrator(new ClassMethodsHydrator(true))
            ->setObject(new Entity())
            ->setAttribute('class', 'element_checkbox-collection')
        ;

        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'add_element_checkbox_value_options',
            'options' => [
                'label' => 'Add New Checkbox Option',
            ],
            'attributes' => [
                'id' => 'add-element_checkbox_value_options',
                'class' => 'btn btn-xs btn-primary btn-element_checkbox_value_options',
                'onClick' => 'javascript:formCreate.add($(this),"element_checkbox_value_options",false,true);return false;',
            ],
        ]);

        $this->add([
            'type' => 'FormGenerator\Form\ElementCheckboxBasicFieldset',
            'name' => 'basic',
            'attributes' => [
                'class' => 'element_checkbox_basic-collection container-element_checkbox_basic-collection',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'value_options',
            'options' => [
                'label' => 'Value Options',
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__index_form_element_checkbox_value_options__',
                'target_element' => [
                    'type' => 'FormGenerator\Form\ElementCheckboxValueOptionsFieldset',
                ],
            ],
            'attributes' => [
                'class' => 'element_checkbox_value_options-collection container-element_checkbox_value_options-collection hidden',
                'data-template-index-placeholder' => '__index_form_element_checkbox_value_options__',
            ],
        ]);

    }

    public function getInputFilterSpecification()
    {
        return [
            'basic' => [
                'required' => false,
            ],
            'value_options' => [
                'required' => false,
            ],
        ];
    }
}
