<?php

namespace FormGenerator\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Zend\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

class ListFiltersAction implements ServerMiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {

        $filter_metadata = [
            'filter_metadata' => [
                'Zend\I18n\Filter\Alnum' => [
                    'allow_white_space' => 'bool',
                    'locale' => 'string',
                ],
                'Zend\I18n\Filter\Alpha' => [
                    'allow_white_space' => 'bool',
                    'locale' => 'string',
                ],
                'Zend\Filter\BaseName' => [],
                'Zend\Filter\Boolean' => [
                    'casting' => 'bool',
                    'type' => 'string',
                ],
                'Zend\Filter\Callback' => [
                    'callback' => 'string',
                ],
                'Zend\Filter\Compress\Bz2' => [
                    'archive' => 'string',
                    'blocksize' => 'int',
                ],
                'Zend\Filter\Compress\Gz' => [
                    'archive' => 'string',
                    'level' => 'int',
                    'mode' => 'string',
                ],
                'Zend\Filter\Compress\Lzf' => [],
                'Zend\Filter\Compress' => [
                    'adapter' => 'string',
                ],
                'Zend\Filter\Compress\Rar' => [
                    'archive' => 'string',
                    'callback' => 'string',
                    'password' => 'string',
                    'target' => 'string',
                ],
                'Zend\Filter\Compress\Snappy' => [],
                'Zend\Filter\Compress\Tar' => [
                    'archive' => 'string',
                    'target' => 'string',
                    'mode' => 'string',
                ],
                'Zend\Filter\Compress\Zip' => [
                    'archive' => 'string',
                    'target' => 'string',
                ],
                'Zend\Filter\DateTimeFormatter' => [
                    'format' => 'string',
                ],
                'Zend\Filter\Decompress' => [
                    'adapter' => 'string',
                ],
                'Zend\Filter\Decrypt' => [
                    'adapter' => 'string',
                ],
                'Zend\Filter\Digits' => [],
                'Zend\Filter\Dir' => [],
                'Zend\Filter\Encrypt\BlockCipher' => [
                    'algorithm' => 'string',
                    'compression' => 'string',
                    'hash' => 'string',
                    'key' => 'string',
                    'key_iteration' => 'int',
                    'vector' => 'string',
                ],
                'Zend\Filter\Encrypt\Openssl' => [
                    'compression' => 'string',
                    'package' => 'bool',
                    'passphrase' => 'string',
                ],
                'Zend\Filter\Encrypt' => [
                    'adapter' => 'string',
                ],
                'Zend\Filter\File\Decrypt' => [
                    'adapter' => 'string',
                    'filename' => 'string',
                ],
                'Zend\Filter\File\Encrypt' => [
                    'adapter' => 'string',
                    'filename' => 'string',
                ],
                'Zend\Filter\File\LowerCase' => [
                    'encoding' => 'string',
                ],
                'Zend\Filter\File\Rename' => [
                    'overwrite' => 'bool',
                    'randomize' => 'bool',
                    'source' => 'string',
                    'target' => 'string',
                ],
                'Zend\Filter\File\RenameUpload' => [
                    'overwrite' => 'bool',
                    'randomize' => 'bool',
                    'target' => 'string',
                    'use_upload_extension' => 'bool',
                    'use_upload_name' => 'bool',
                ],
                'Zend\Filter\File\UpperCase' => [
                    'encoding' => 'string',
                ],
                'Zend\Filter\HtmlEntities' => [
                    'charset' => 'string',
                    'doublequote' => 'bool',
                    'encoding' => 'string',
                    'quotestyle' => 'int',
                ],
                'Zend\Filter\Inflector' => [
                    'throwTargetExceptionsOn' => 'bool',
                    'targetReplacementIdentifier' => 'string',
                    'target' => 'string',
                ],
                'Zend\Filter\Int' => [],
                'Zend\Filter\Null' => [
                    'type' => 'int|string',
                ],
                'Zend\I18n\Filter\NumberFormat' => [
                    'locale' => 'string',
                    'style' => 'int',
                    'type' => 'int',
                ],
                'Zend\I18n\Filter\NumberParse' => [
                    'locale' => 'string',
                    'style' => 'int',
                    'type' => 'int',
                ],
                'Zend\Filter\PregReplace' => [
                    'pattern' => 'string',
                    'replacement' => 'string',
                ],
                'Zend\Filter\RealPath' => [
                    'exists' => 'bool',
                ],
                'Zend\Filter\StringToLower' => [
                    'encoding' => 'string',
                ],
                'Zend\Filter\StringToUpper' => [
                    'encoding' => 'string',
                ],
                'Zend\Filter\StringTrim' => [
                    'charlist' => 'string',
                ],
                'Zend\Filter\StripNewlines' => [],
                'Zend\Filter\StripTags' => [
                    'allowAttribs' => 'string',
                    'allowTags' => 'string',
                ],
                'Zend\Filter\ToInt' => [],
                'Zend\Filter\ToNull' => [
                    'type' => 'int|string',
                ],
                'Zend\Filter\UriNormalize' => [
                    'defaultscheme' => 'string',
                    'enforcedscheme' => 'string',
                ],
                'Zend\Filter\Word\CamelCaseToDash' => [],
                'Zend\Filter\Word\CamelCaseToSeparator' => [
                    'separator' => 'string',
                ],
                'Zend\Filter\Word\CamelCaseToUnderscore' => [],
                'Zend\Filter\Word\DashToCamelCase' => [],
                'Zend\Filter\Word\DashToSeparator' => [
                    'separator' => 'string',
                ],
                'Zend\Filter\Word\DashToUnderscore' => [],
                'Zend\Filter\Word\SeparatorToCamelCase' => [
                    'separator' => 'string',
                ],
                'Zend\Filter\Word\SeparatorToDash' => [
                    'separator' => 'string',
                ],
                'Zend\Filter\Word\SeparatorToSeparator' => [
                    'searchseparator' => 'string',
                    'replacementseparator' => 'string',
                ],
                'Zend\Filter\Word\UnderscoreToCamelCase' => [],
                'Zend\Filter\Word\UnderscoreToDash' => [],
                'Zend\Filter\Word\UnderscoreToSeparator' => [
                    'separator' => 'string',
                ],
            ],
        ];

        return new JsonResponse($filter_metadata);
    }
}
